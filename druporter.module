<?php

/**
 * @file
 *
 * Main file of the Druporter module configuration.
 */

/**
 * Implements hook_menu().
 */
function druporter_menu() {

  $items = array();
  $items['admin/config/system/druporter'] = array(
    'title' => 'Druporter settings',
    'description' => 'Configure the Druporter module.',
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('druporter_admin_settings_form'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function druporter_permission() {
  return array(
    'administer druporter' => array(
      'title' => t('Administer Druporter'),
      'description' => t('Administer Druporter module.'),
    ),
  );
}

/**
 * Implements hook_help().
 */
function druporter_help($path, $arg) {
  switch ($path) {
    case 'admin/help#druporter':
      module_load_include('inc', 'druporter');
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides a mechanism to import page data from other webiste and create the respective page.') . '</p>';
      $output .= '<h3>' . t('Settings') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Test Content') . '</dt>';
      $output .= '<dd>' . t('Test Content') . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_settings().
 */
function druporter_admin_settings_form() {
  $form['druporter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Druporter settings'),
    '#description' => t('Put the required information into the fields to make the module functional.'),
    '#collapsible' => FALSE,
  );

  $form['druporter']['druporter_source_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Source/Old Website URL'),
    '#default_value' => variable_get("druporter_source_url", ""),
    '#description' => t('Enter the URL of the Source/Old Website.'),
    '#required' => TRUE,
  );

  $form['druporter']['druporter_choose_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content Type'),
    '#multiple' => FALSE,
    '#options' => node_type_get_names(),
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'druporter_replace_field_callback',
      'method' => 'replace',
      'wrapper' => 'dependent-select-body-fields',
    ),
    '#default_value' => variable_get("druporter_choose_content_type", ""),
    '#description' => t('Choose the content type to port old pages into.'),
    '#required' => TRUE,
  );

  $form['druporter']['druporter_title_field'] = array(
    '#type' => 'select',
    '#title' => t('Title Field'),
    '#multiple' => FALSE,
    '#options' => array('title' => 'Title'),
    '#default_value' => variable_get("druporter_title_field", ""),
    '#description' => t('Choose the field to port old pages titles into.'),
    '#required' => TRUE,
  );

  $form['druporter']['druporter_title_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Title Field Selector'),
    '#default_value' => variable_get("druporter_title_selector", ""),
    '#description' => t('Enter the selector (CSS Selector) for the title content on the old pages. <br />Ensure that the selector yields unique DOM elements only.'),
    '#required' => TRUE,
  );

  $form['druporter']['druporter_body_field'] = array(
    '#type' => 'select',
    '#title' => t('Body Field'),
    '#multiple' => FALSE,
    '#options' => druporter_get_entity_fields("node", variable_get("druporter_choose_content_type")),
    '#default_value' => variable_get("druporter_body_field", ""),
    '#prefix' => '<div id="dependent-select-body-fields">',
    '#suffix' => '</div>',
    '#description' => t('Choose the field to port old pages body content into.'),
    '#required' => TRUE,
  );

  $form['druporter']['druporter_body_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Body Field Selector'),
    '#default_value' => variable_get("druporter_body_selector", ""),
    '#description' => t('Enter the selector (CSS Selector) for the body content on the old pages. <br />Ensure that the selector yields unique DOM elements only.'),
    '#required' => TRUE,
  );

  $form['druporter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#validate'][] = 'druporter_validate';

  return system_settings_form($form);
}


/**
 * Implements hook_validate().
 */
function druporter_validate($form, $form_state) {

  // Custom url validation.
  if (!(empty($form_state['values']['druporter_source_url'])) && !(filter_var($form_state['values']['druporter_source_url'], FILTER_VALIDATE_URL))) {
    form_set_error("druporter_source_url", t("Source/Old Website URL must be a valid URL."));
  }
}

/**
 * Implements druporter_replace_field_callback($form, &$form_state).
 */
function druporter_replace_field_callback($form, &$form_state) {

  $fields_list = druporter_get_entity_fields("node", $form_state['values']['druporter_choose_content_type']);
  $form['druporter']['druporter_body_field']['#options'] = $fields_list;

  return $form['druporter']['druporter_body_field'];
}

/**
 * Implements druporter_get_entity_fields($entity_type, $node_type).
 */
function druporter_get_entity_fields($entity_type, $node_type) {

  $node_type = empty($node_type) ? 'article' : $node_type;
  $replaced_tag = 'field_';
  $replaced_with = '';
  $fields = field_read_fields(array(
      'entity_type' => $entity_type,
      'bundle' => $node_type,
    ));
  foreach ($fields as $key => $field) {
    if (in_array($field['type'], array('text_with_summary', 'text_long'))) {
      $field_lists[$key] = ucwords(str_replace('_', ' ', str_replace($replaced_tag, $replaced_with, $key)));
    }
  }

  return $field_lists;
}

/**
 * Implements druporter_import_content($url).
 */
function druporter_import_content($url) {

  // Includes phpQuery library.
  _druporter_pq_inc();

  $headers = array();
  $page_data = drupal_http_request($url, $headers, 'GET');

  if (empty($page_data) || !in_array($page_data->code, array('200', '201'))) {
    return array('error' => TRUE);
  }

  $_content = phpQuery::newDocument($page_data->data);
  $title = $_content->find(variable_get('druporter_title_selector'))->text();
  $body = $_content->find(variable_get('druporter_body_selector'))->html();

  druporter_add_node($title, $body);

  return array('error' => FALSE, 'body' => $body, 'node' => $title);
}

/**
 * Implements druporter_add_node($title, $body).
 */
function druporter_add_node($title, $body) {

  $time = time();
  $content_type = variable_get("druporter_choose_content_type");
  $title_field = variable_get("druporter_title_field");

  $node_data_array = array(
    "type" => $content_type,
    "language" => 'und',
    $title_field => $title,
    "uid" => 1,
    "status" => 1,
    "created" => $time,
    "changed" => $time,
    "comment" => 0,
    "promote" => 0,
    "sticky" => 0,
    "tnid" => 0,
    "translate" => 0,
  );

  $nid = db_insert('node')->fields($node_data_array)->execute();

  $node_data_rev_array = array(
    "nid" => $nid,
    "uid" => 1,
    "title" => $title,
    "log" => '',
    "timestamp" => $time,
    "status" => 1,
    "comment" => 0,
    "promote" => 0,
    "sticky" => 0,
  );

  $vid = db_insert('node_revision')->fields($node_data_rev_array)->execute();

  db_update('node')->fields(array('vid' => $vid))->condition('nid', $nid, '=')->execute();

  $body_data_array = array(
    "entity_type" => 'node',
    "bundle" => $content_type,
    "deleted" => 0,
    "entity_id" => $nid,
    "revision_id" => $vid,
    "language" => 'und',
    "delta" => '0',
    "body_value" => $body,
    "body_summary" => '',
    "body_format" => 'full_html',
  );

  $body_data_rev_array = $body_data_array;

  db_insert('field_data_body')->fields($body_data_array)->execute();
  db_insert('field_revision_body')->fields($body_data_rev_array)->execute();

  $path_data_array = array(
    "pid" => $nid,
    "source" => 'node/' . $nid,
    "alias" => request_path(),
    "language" => 'und',
  );

  db_insert('url_alias')->fields($path_data_array)->execute();

  return $nid;
}

/**
 * Implements druporter_init().
 */
function druporter_init() {

  if (!menu_get_item($_GET['q'])) {
    global $base_url;
    $request_path = request_path();
    $druporter_target_url = rtrim(variable_get('druporter_source_url'), '/') . '/' . ltrim($request_path, '/');
    $http_response = druporter_import_content($druporter_target_url);
    if (isset($http_response['error']) && empty($http_response['error'])) {
      drupal_set_message(t('Your content has been imported successfully from <a href="!url" target="_blank">!url</a>.', array("!url" => url($druporter_target_url))), 'status');
      drupal_goto(rtrim($base_url, '/') . '/' . ltrim($request_path, '/'));
    }
  }
}

/**
 * Finds and loads the phpQuery library.
 */
function _druporter_pq_inc() {
  static $loaded = NULL;

  if (!isset($loaded)) {

    // Locations to check for phpQuery, by order of preference.
    $include_locations = array();

    // Ensure libraries module is loaded.
    module_load_include('module', 'libraries');

    if (function_exists('libraries_get_path')) {
      // Add libraries supported path.
      $include_locations[] = libraries_get_path('pq') . '/phpQuery.php';
    }

    foreach ($include_locations as $include_location) {
      if (is_file($include_location)) {
        require_once $include_location;
        break;
      }
    }

    $loaded = function_exists('pq');
  }

  return $loaded;
}
