Druporter - The convenient Drupal importer module
=================================================

The Druporter module can be used to seamlessly import contents from an
old website (non-Drupal based) to a newly created or existing Drupal website.

How it works
------------

Druporter works on the following model

* Druporter is a quick and easy module to import contents from any legacy 
website irrespective of the back-end framework used by the website.
* Druporter does a page by page on-demand import rather than a bulk import.
* A user requests for a page on the new website which has not been imported
from the old website yet.
(Eg. http://my.new-drupal-site.com/path-to-my-old-content)
* If the page has already been imported, Drupal displays the imported page.
* If it has not been imported, Druporter requests the page to the old 
website i.e. to the URL http://my.old-site.com/path-to-my-old-content.
* Next, Druporter fetches the content and title from the page and uses them
to create the node of the selected content type (in Druporter settings).
* Druporter retains the old website's path aliases.
* So, next time if some one requests for the page
http://my.new-drupal-site.com/path-to-my-old-content, they receive the
node imported Drupal by Druporter.

Here is a diagram explaining the working model:

---

  ![How it works](https://www.drupal.org/files/druporter-working.jpg)

---

Advantages
----------

* Permalinks are retained while importing, so SEO is not affected.
* It is completely seamless and on-demand.
* No manual work of sanitising import dumps has to be done.
* Re-importing a specific content page is as easy as just deleting the
node / page from the admin panel and requesting the URL again.

Installation
------------

* Download and extract the module archive into
`<drupal_root>/sites/all/modules/`
* Download `phpQuery.php` from
[here](https://github.com/nettantra/phpQuery-lib)
and place it inside `<drupal_root>/sites/all/libraries/pq/` so that its
path looks like `<drupal_root>/sites/all/libraries/pq/phpQuery.php`.

    Alternatively, use the following commands:

        mkdir -p <drupal_root>/sites/all/libraries
        cd <drupal_root>/sites/all/libraries
        git clone https://github.com/nettantra/phpQuery-lib.git pq

* Go to `Drupal Admin >> Modules` i.e. `(admin/modules)`
* Select Druporter and click save
* Go to `Drupal Admin >> Configuration >> System >> Druporter Settings`
i.e. `(admin/config/system/druporter)`
* Enter the Source/Old Website URL, the Content Type to populate to,
the title and body fields on the content type,
the selectors for the title and body fields and Save
* Now, simply go to your new site and import old contents
using old content permalinks on the new site:

        Eg. http://my.new-drupal-site.com/path-to-my-old-content 
        will import data from http://my.old-site.com/path-to-my-old-content

Module Dependencies
-------------------

* [ctools](https://www.drupal.org/project/ctools)
* [pathauto](https://www.drupal.org/project/pathauto)
* [libraries](https://www.drupal.org/project/libraries)

External Libraries
------------------

* [phpQuery](https://github.com/nettantra/phpQuery-lib)

Sponsored By
------------

* [NetTantra Technologies](http://www.nettantra.com/)

---
